require "rake_deploy_lib/version"

# RakeDeployLib includes methodes for deploying und syncing files to a remote host.
# The Gem depends on rsync! The OS (Operating System) needs support for the rsync command (e.q. Linux, Unix, OS X).
# * deploy = transfare local (repository) files to remote server
# * sync = transfare remote server files to local (repository)
module RakeDeployLib

  # Deploys local (repository) files, excepts excludes, to the remote server.
  # Please be aware, that files will be deleted at remote host, if they don't exist local! With excludes you are able to prevent deletion.
  # * local (mendetory) - directory relative to the project root without / (slash) at the end.
  # * user (mendetory) - ssh user name at the remote server. Use of ssh certificate authentication is recommanded.
  # * host (mendetory) - server domain like example.com
  # * remote (mendetory) - absolute directory path without / (slash) at the end.
  # * excludes (optional) - an arrey of files or directories to exclude.
  #   * Files: e.g. "myfile.txt" or ".gitignore"
  #   * Directories: e.g. "mydirectory/" be aware of the trailing / (slash)
  def RakeDeployLib.deploy(local, user, host, remote, excludes = "")
    if (!local || !user || !host || !remote)
      return false
    end
    cmd_rsync = "rsync -rz --delete "
    cmd_exclude = ""

    if excludes != ""
      excludes.each do |to_exclude|
        cmd_exclude = "#{cmd_exclude}--exclude=#{to_exclude} "
      end
      cmd_rsync = "#{cmd_rsync}#{cmd_exclude}"
    end

    if (system "#{cmd_rsync}#{local}/ #{user}@#{host}:#{remote}/")
      return true
    else
      return false
    end
  end

  # Retuns a formated title. You can use this title to output formated messages.
  # Example call: RakeDeployLib.task_delimiter("Install npm packages", 3, "*", "-")
  # Example output:
  # ' ************************ '
  # ' * Install npm packages * '
  # ' ------------------------ '
  #
  # * task_title (mendotory) - string "Install npm packages"
  # * line_break_delimiter (optional) - line breaks before output Title as integer (default is 3)
  # * header_char (optional) - one character as upper frame (default is *)
  # * footer_char (optional) - one character as bottom frame (default is -)
  #
  # returns a string with line breaks
  def RakeDeployLib.task_delimiter(task_title, line_break_delimiter=3, header_char = "*", footer_char = "-")
    raise "\n\n\nTask tile missing!" if !task_title
    output = ""
    line_break_delimiter.times{output += "\n"}
    width_count = task_title.length + 4
    width_count.times{output += "#{header_char}"}
    output += "\n" + "#{header_char} #{task_title} #{header_char}" + "\n"
    width_count.times{output += "#{footer_char}"}
    output += "\n"
    return output
  end

  # Outputs a question to the user and prompst for answer with yes.
  # User are able to use "y", "Y", "yes", "Yes", "YES" as positiv answer.
  # Example call: RakeDeployLib.are_you_sure("Are you sure you want to deploy? (yes|no):")
  #
  # * question (optional) - string (default is "Are you sure? (yes|no):")
  #
  # If answer is not yes, an exit command will stop the whole script.
  # Methode uses RakeDeployLib.task_delimiter for prompting the question
  def RakeDeployLib.are_you_sure(question = "Are you sure? (yes|no):")
    yes_prompt = ["y", "Y", "yes", "Yes", "YES"]
    puts RakeDeployLib.task_delimiter(question, 6, "-")
    user_prompt = STDIN.gets.chomp
    if !yes_prompt.include?(user_prompt)
      puts "You answered with: #{user_prompt}"
      puts "Me, my self and I will stop now."
      puts ""
      exit
    end
  end
end
