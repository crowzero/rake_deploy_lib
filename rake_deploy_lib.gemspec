# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rake_deploy_lib/version'

Gem::Specification.new do |spec|
  spec.name          = "rake_deploy_lib"
  spec.version       = RakeDeployLib::VERSION
  spec.authors       = ["Ulrich Lutz"]
  spec.email         = ["rake_deploy_lib@lutz-clan.de"]

  spec.summary       = "RakeDeployLib includes methods for deploying und syncing files to a remote host."
  spec.description   = "Before Version 1.0.0 the lib is still in progress and will continusly extanded with methodes. Published methodes are all tested with unittest. RakeDeployLib includes methods for deploying und syncing files to a remote host. The Gem depends on rsync! The OS (Operating System) needs support for the rsync command (e.q. Linux, Unix, OS X). Deploy = transfare local (repository) files to remote server. Sync = transfare remote server files to local (repository). Please follow the Documentation and Sourcecode links too."
  spec.homepage      = "https://github.com/crowzero/rake_deploy_lib"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
