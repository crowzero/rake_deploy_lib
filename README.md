# RakeDeployLib

RakeDeployLib includes methods for deploying und syncing files to a remote host.
The Gem depends on rsync! The OS (Operating System) needs support for the rsync command (e.q. Linux, Unix, OS X).
* deploy = transfare local (repository) files to remote server
* sync = transfare remote server files to local (repository)

## Status
This is an alpha stage version of the Gem. More methods are follow.
[![Gem Version](https://badge.fury.io/rb/rake_deploy_lib.svg)](https://badge.fury.io/rb/rake_deploy_lib)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rake_deploy_lib'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rake_deploy_lib

## Usage
### .deploy

```
RakeDeployLib.deploy(@local, @user, @host, @remote, @excludes)
```

Deploys local (repository) files, excepts excludes, to the remote server.
Please be aware, that files will be deleted at remote host, if they don't exist local! With excludes you are able to prevent deletion.
* local (mendetory) - directory relative to the project root without / (slash) at the end.
* user (mendetory) - ssh user name at the remote server. Use of ssh certificate authentication is recommanded.
* host (mendetory) - server domain like example.com
* remote (mendetory) - absolute directory path without / (slash) at the end.
* excludes (optional) - an arrey of files or directories to exclude.
* Files: e.g. "myfile.txt" or ".gitignore"
* Directories: e.g. "mydirectory/" be aware of the trailing / (slash)

### .task_delimiter
Retuns a formated title. You can use this title to output formated messages.

**Example**
```
RakeDeployLib.task_delimiter(task_title, line_break_delimiter=3, header_char = "*", footer_char = "-")
RakeDeployLib.task_delimiter("Install npm packages", 3, "*", "-")
```

Output:
```
************************
* Install npm packages *
------------------------
```
**Paramters**
* task_title (mendotory) - string "Install npm packages"
* line_break_delimiter (optional) - line breaks before output Title as integer (default is 3)
* header_char (optional) - one character as upper frame (default is *)
* footer_char (optional) - one character as bottom frame (default is -)

**Return**
Returns a string with line breaks

### .are_you_sure
Outputs a question to the user and prompst for answer with yes.
User are able to use "y", "Y", "yes", "Yes", "YES" as positiv answer.

**Example**
```
RakeDeployLib.are_you_sure("Are you sure you want to deploy? (yes|no):")
```
**Paramters**
* question (optional) - string (default is "Are you sure? (yes|no):")

**Return**
If answer is not yes, an exit command will stop the whole script.

**Dependencies**
Methode uses RakeDeployLib.task_delimiter for prompting the question

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/rake_deploy_lib. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
