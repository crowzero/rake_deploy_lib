# require 'spec_helper'
require 'rake_deploy_lib'
require 'net/ssh'

describe RakeDeployLib, "- Base modul test" do
  before :each do
    	@user = "radeli"
      @host = "ori.lc.private"
      @remote = "/home/radeli/unittest-radeli"
      @local_empty = "etc/unittest_directories/01_empty"
      @local_regular = "etc/unittest_directories/02_regular"
      @local_excludes_local = "etc/unittest_directories/03_excludes_local"
      @local_excludes_remote = "etc/unittest_directories/04_excludes_remote"
      #@local_includes =  "etc/unittest_directories/11_includes"
      @local_includes =  "/Users/u.lutz/Documents/EWS/Customers/Siemens/next47/src"
      @excludes_default = [".DS_Store", ".gitignore", ".git/"]
      @excludes_local = @excludes_default +  ["foo02.txt"]
      @excludes_remote = @excludes_default + ["foodir/"]
      @includes =  ["typo3conf/*/*"]
	end

  describe "Check for version number" do
    it 'has a version number' do
      expect(RakeDeployLib::VERSION).not_to be nil
    end
  end

  describe "RakeDeployLib.deploy" do
    it 'empty' do
      expect(RakeDeployLib.deploy(@local_empty, @user, @host, @remote, @excludes_default)).to eq(true)
      expect(get_files_remote).to eq("")
    end
    it 'regular' do
      expect(RakeDeployLib.deploy(@local_regular, @user, @host, @remote, @excludes_default)).to eq(true)
      expect(get_files_remote).to eq("#{@remote}/foodir/foo11.txt\n#{@remote}/foo01.txt\n")
    end
    it 'excludes local' do
      expect(RakeDeployLib.deploy(@local_excludes_local, @user, @host, @remote, @excludes_local)).to eq(true)
      expect(get_files_remote).to eq("#{@remote}/foodir/foo11.txt\n#{@remote}/foo01.txt\n")
    end
    it 'excludes remote' do
      expect(RakeDeployLib.deploy(@local_excludes_remote, @user, @host, @remote, @excludes_remote)).to eq(true)
      expect(get_files_remote).to eq("#{@remote}/foodir/foo11.txt\n#{@remote}/foo01.txt\n#{@remote}/foo02.txt\n")
    end
  end

  describe "RakeDeployLib.task_delimiter" do
    it 'with line_break_delimiter' do
      expect(RakeDeployLib.task_delimiter("Task title")).to eq("\n\n\n**************\n* Task title *\n--------------\n")
    end
  end

  #
  # describe "RakeDeployLib.are_you_sure" do
  #   it 'Ask question' do
  #     expect(RakeDeployLib.are_you_sure("Question title")).to eq("\n\n\n******************\n*** Task title ***\n------------------\n")
  #   end
  # end
end

def get_files_remote
  stdout = ""
  Net::SSH.start(@host, @user) do |ssh|
    # capture only stdout matching a particular pattern
    ssh.exec!("find #{@remote} -type f") do |channel, stream, data|
      stdout << data if stream == :stdout
    end
  end
  return stdout
end
